<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get('/','HomeController@index')->name('home');

// Add new routes for admin login, registration, forgot and reset password
Route::get('/admin','Adminauth\LoginController@showLoginForm');
Route::get('/admin/login','Adminauth\LoginController@showLoginForm');
Route::post('/admin/login','Adminauth\LoginController@adminLogin');
Route::get('/admin/register','Adminauth\LoginController@showRegistrationForm');
Route::post('/admin/register','Adminauth\RegisterController@register');

// Add new routes for admin
Route::group(['prefix' => '/admin'],function(){
    Route::get('/','admin\DashboardController@index');
	Route::get('/dashboard','admin\DashboardController@index');
});

// Add new route for 'admin' middleware
Route::group(['middleware' => ['admin']],function(){
	// Logout routes
    Route::post('/admin/logout','Adminauth\LoginController@logout');
	Route::get('/admin/logout','Adminauth\LoginController@logout');
	
	//Admin Module Routes
	Route::resource('admin/cms', 'admin\CMSController',['except' => ['destroy']]);
	
	//Common Delete Route
	Route::get('admin/delete/{controller?}/{table?}/{field?}/{id?}', 'admin\Controller@generaldelete')->where(['table' => '[a-z_]+', 'id' => '[0-9]+']);*/
});