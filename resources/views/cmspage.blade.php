@extends('layouts.app')

@section('content')
<!-- container	-->
<section class="cms-bg">
  <div class="container">
    <section class="cmscontaint-part">
      @if($cmsdata->cms_content_image != '' || $cmsdata->cms_content_image != NULL)
      	<div class="col-sm-4 cmscontent-image-container">
      		<img src="{{ asset('images/cmspages/'.$cmsdata->cms_page_id.'/'.$cmsdata->cms_content_image) }}" title="" class="img-responsive cmscontent-image">
      	</div>
      @endif
      <div @if($cmsdata->cms_content_image == '' || $cmsdata->cms_content_image == NULL) class="col-sm-12" @else class="col-sm-8 cmsmaincontent" @endif>
        <div class="cmsdesc_container">
          <h1>{{ $cmsdata->cms_content_title }}</h1>
          <div class="cmspage_content">
          	{!! $cmsdata->cms_page_content !!}
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </section>
  </div>
</section>
@endsection
